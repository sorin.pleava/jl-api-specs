<?php

declare(strict_types=1);

namespace App\Form\Type;

use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BaseType extends AbstractType
{
    protected FormBuilderInterface $builder;
    protected array $properties;

    /**
     * @param array $properties
     */
    public function setApiModelProperties(array $properties): void
    {
        $this->properties = $properties;
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function setBuilder(FormBuilderInterface $builder): void
    {
        $this->builder = $builder;
    }

    /**
     * @throws Exception
     */
    public function addForm(string $child, string $type, array $options): self
    {
        if (empty($this->builder)) {
            throw new Exception('Builder not set');
        }

        if (!in_array($child, $this->properties)) {
            throw new Exception('Invalid property');
        }

        $this->builder->add($child, $type, $options);

        return $this;
    }
}
