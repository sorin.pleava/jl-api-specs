<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Customer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;

class  CustomerType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->setBuilder($builder);
        $this->setApiModelProperties(\DemoClient\Model\Customer::attributeMap());
        $this
            ->addForm('email', EmailType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Email can not be blank',
                    ]),
                    new Email(),
                ]
            ])
            ->addForm('phoneNumber', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
