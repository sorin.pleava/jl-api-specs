import React, {useState, useEffect} from "react";
import ProductService from "./ProductService";
import {
    Dialog, DialogContent, DialogTitle, Grid,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField,
    Typography
} from "@material-ui/core";
import Add from "@material-ui/icons/Add";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {AddProductRequestBody} from "sdk-js/src";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function Product() {
    const [products, setProducts] = useState([]);
    const [open, setOpen] = useState(false);
    const [code, setCode] = useState(null);
    const [title, setTitle] = useState(null);
    const [price, setPrice] = useState(null);

    const getProducts = () => {
        (new ProductService()).getProducts().then((response) => {
            setProducts(response.response.body);
        }).catch(error => {
            console.log(error);
        })
    }

    useEffect(() => {
        getProducts();
    },[])

    const handleAddButton = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
        setCode(null);
        setTitle(null);
        setPrice(null);
    }

    const handleSave = () => {
        let data = new AddProductRequestBody();
        data.code = code;
        data.title = title;
        data.price = price;

        console.log(data);
        (new ProductService()).createProduct({'addProductRequestBody':data}).then((response) => {
            let newList = [...products, response.response.body];
            setProducts(newList); //for reload the table
        }).catch(error => {
            console.log(error);
        })
        setOpen(false);
        setCode(null);
        setTitle(null);
        setPrice(null);
    }

    const classes = useStyles();

    return (
        <React.Fragment>
            <IconButton onClick={() => handleAddButton()}>
                <Typography>Add new product</Typography><Add/>
            </IconButton>

            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Code</TableCell>
                            <TableCell align="right">Title</TableCell>
                            <TableCell align="right">Price</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {products.map((row) => (
                            <TableRow key={row.id}>
                                <TableCell align="right">{row.code}</TableCell>
                                <TableCell align="right">{row.title}</TableCell>
                                <TableCell align="right">{row.price}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle><Typography variant={"h5"}>Add Customer</Typography></DialogTitle>
                <DialogContent>
                    <Grid container direction="column" spacing={3}>
                        <Grid item xs={12}>
                            <TextField id="standard-basic" label="Code" value={code}
                                       onChange={(e) => {
                                           setCode(e.target.value)
                                       }}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField id="standard-basic" label="Title" value={title}
                                       onChange={(e) => {
                                           setTitle(e.target.value)
                                       }}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField id="standard-basic" label="Price" value={price}
                                       onChange={(e) => {
                                           setPrice(e.target.value)
                                       }}/>
                        </Grid>
                        <Grid item container direction="row" xs={12}>
                            <Grid item xs={6}>
                                <Button variant="contained" color="primary" onClick={handleSave}>Save</Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button variant="contained" color="primary" onClick={handleClose}>Cancel</Button>
                            </Grid>
                        </Grid>
                    </Grid>

                </DialogContent>
            </Dialog>
        </React.Fragment>
    );
}

export default Product;
