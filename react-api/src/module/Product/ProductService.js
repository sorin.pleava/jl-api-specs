import ProductsApi from "sdk-js/src/Api/ProductsApi";
import ApiService from "../../library/ApiService";

export default class ProductService extends ApiService{
    getProductsApi() {
        if (!this.customersApi) {
            this.customersApi = new ProductsApi(this.apiClient);
        }
        return this.customersApi;
    }

    getProducts() {
        return this.getProductsApi().getProductsWithHttpInfo();
    }

    getProductById(id) {
        return this.getProductsApi().getProductByIdWithHttpInfo(id);
    }

    createProduct(data) {
        return this.getProductsApi().createProductWithHttpInfo(data);
    }

    deleteProduct(id) {
        return this.getProductsApi().deleteProductByIdWithHttpInfo(id);
    }
}
