import React, {useState, useEffect} from "react";
import CustomerService from "./CustomerService";
import {
    Checkbox,
    Dialog,
    DialogContent, DialogTitle,
    Fab, Grid,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField, Typography
} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';
import Add from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';

import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AddCustomerRequestBody from "sdk-js/src/Model/AddCustomerRequestBody";
import ProductService from "../Product/ProductService";
import CartService from "../Cart/CartService";
import Cart from "sdk-js/src/Model/Cart";
import AddCartRequestBody from "sdk-js/src/Model/AddCartRequestBody";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function Customer() {
    const [customers, setCustomers] = useState([]);
    const [open, setOpen] = useState(false);
    const [email, setEmail] = useState(null);
    const [phoneNumber, setPhoneNumber] = useState(null);
    const [selectedId, setSelectedId] = useState(null);
    const [openCart, setOpenCart] = useState(false);
    const [products, setProducts] = useState([]);
    const [customerProducts, setCustomerProducts] = useState([]);
    const [selected, setSelected] = useState([]);

    const getCustomers = () => {
        (new CustomerService()).getCustomers().then((response) => {
            setCustomers(response.response.body);
            console.log(response);
        }).catch(error => {
            console.log(error);
        })
    }

    const getCustomerProducts = (id) => {
        (new CartService()).getCustomerCart(id).then((response) => {
            setCustomerProducts(response.response.body);
            console.log(response);
        }).catch(error => {
            setCustomerProducts([]);
            console.log(error);
        })
    }

    const getProducts = () => {
        (new ProductService()).getProducts().then((response) => {
            setProducts(response.response.body);
            console.log(response);
        }).catch(error => {
            console.log(error);
        })
    }


    useEffect(() => {
        getCustomers();
        getProducts();
    }, [])


    const handleCartButton = (id) => {
        setOpenCart(true);
        getCustomerProducts(id);
        setSelectedId(id);
    }

    const handleAddButton = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
        setEmail('');
        setPhoneNumber('');
    }

    const handleCloseCart = () => {
        setOpenCart(false);
        setSelected([]);
    }

    const handleSaveCart = (customerId) => {
        getCustomerProducts(customerId);
        console.log(customerId);

        console.log(customerProducts);
        if (customerProducts.length === 0) {
            let data = new AddCartRequestBody();
            data.products = selected;
            (new CartService()).createCart(customerId, {'addCartRequestBody': data}).then((response) => {
                console.log(response);
            }).catch(error => {
                console.log(error);
            })
        } else {
            let data = new Cart();
            data.products = selected;
            (new CartService()).updateCart(customerId, {'cart': data}).then((response) => {
                console.log(response);
            }).catch(error => {
                console.log(error);
            })
        }
        setOpenCart(false);
        setSelected([]);
    }

    const handleSave = () => {
        let data = new AddCustomerRequestBody();
        data.email = email;
        data.phoneNumber = phoneNumber;

        (new CustomerService()).createCustomer({'addCustomerRequestBody': data}).then((response) => {
            let newList = [...customers, response.response.body];
            setCustomers(newList); //for reload the table
        }).catch(error => {
            console.log(error);
        })
        setOpen(false);
        setEmail('');
        setPhoneNumber('');
    }

    const classes = useStyles();
    const isSelected = (id) => selected.indexOf(id) !== -1;

    const handleClick = (event, id) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelectedIds = products.map((n) => n.id);
            setSelected(newSelectedIds);
            return;
        }
        setSelected([]);
    }

    return (
        <React.Fragment>
            <IconButton onClick={() => handleAddButton()}>
                <Typography>Add new customer</Typography><Add/>
            </IconButton>

            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Email</TableCell>
                            <TableCell align="right">Phone Number</TableCell>
                            <TableCell align="right">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {customers.map((row) => (
                            <TableRow key={row.id}>
                                <TableCell align="right">{row.email}</TableCell>
                                <TableCell align="right">{row.phoneNumber}</TableCell>

                                <TableCell align="right">
                                    <IconButton onClick={() => handleCartButton(row.id)}>
                                        <ShoppingCartIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle><Typography variant={"h5"}>Add Customer</Typography></DialogTitle>
                <DialogContent>
                    <Grid container direction="column" spacing={3}>
                        <Grid item xs={12}>
                            <TextField id="standard-basic" label="Email" value={email}
                                       onChange={(e) => {
                                           setEmail(e.target.value)
                                       }}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField id="standard-basic" label="Phone number" value={phoneNumber}
                                       onChange={(e) => {
                                           setPhoneNumber(e.target.value)
                                       }}/>
                        </Grid>

                        <Grid item container direction="row" xs={12}>
                            <Grid item xs={6}>
                                <Button variant="contained" color="primary"
                                        onClick={handleSave}>Save</Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button variant="contained" color="primary" onClick={handleClose}>Cancel</Button>
                            </Grid>
                        </Grid>
                    </Grid>

                </DialogContent>
            </Dialog>

            <Dialog onClose={handleCloseCart} aria-labelledby="customized-dialog-title" open={openCart}>
                <DialogContent>
                    <Grid item container direction="row" xs={12} spacing={3}>

                        <Grid item xs={12}>
                            <Typography variant={"h5"}>Available products</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell padding="checkbox">
                                            <Checkbox
                                                color={"primary"}
                                                indeterminate={selected.length > 0 && selected.length < products.length}
                                                checked={products.length > 0 && selected.length === products.length}
                                                onChange={handleSelectAllClick}
                                                inputProps={{'aria-label':'select all products'}}
                                            />
                                        </TableCell>                                        <TableCell align="left">Code</TableCell>
                                        <TableCell align="left">Title</TableCell>
                                        <TableCell align="left">Price</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {products.length > 0 ?
                                    products.map((row) => {
                                        const isItemSelected = isSelected(row.id) ||
                                            (customerProducts[0] !== undefined && customerProducts[0].products.find(obj=>obj.id === row.id));
                                        const labelId = `enhanced-table-checkbox-${row.id}`;
                                        return <TableRow
                                            key={row.id}
                                            onClick={(event) => handleClick(event, row.id)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            selected={isItemSelected}
                                        >
                                            <TableCell>
                                                <Checkbox
                                                    color={"primary"}
                                                    checked={isItemSelected}
                                                    inputProps={{'aria-labelledby':labelId}}
                                                    />
                                            </TableCell>
                                            <TableCell align="left">{row.code}</TableCell>
                                            <TableCell align="left">{row.title}</TableCell>
                                            <TableCell align="left">{row.price}</TableCell>
                                        </TableRow>
                                    }):''}
                                </TableBody>
                            </Table>
                        </Grid>
                        <Grid item container direction="row" xs={12}>
                            <Grid item xs={6}>
                                <Button variant="contained" color="primary" onClick={() => handleSaveCart(selectedId)}>Add to cart</Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button variant="contained" color="primary" onClick={handleCloseCart}>Cancel</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent>
            </Dialog>
        </React.Fragment>
    );
}

export default Customer;
