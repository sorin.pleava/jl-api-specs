import CustomersApi from "sdk-js/src/Api/CustomersApi";
import ApiService from "../../library/ApiService";

export default class CustomerService extends ApiService{
    getCustomersApi() {
        if (!this.customersApi) {
            this.customersApi = new CustomersApi(this.apiClient);
        }
        return this.customersApi;
    }

    getCustomers() {
        return this.getCustomersApi().getCustomersWithHttpInfo();
    }

    getCustomerById(id) {
        return this.getCustomersApi().getCustomerByIdWithHttpInfo(id);
    }

    createCustomer(data) {
        return this.getCustomersApi().createCustomerWithHttpInfo(data);
    }

    updateCustomer(id, data) {
        return this.getCustomersApi().updateCustomerWithHttpInfo(id, data);
    }

    deleteCustomer(id) {
        return this.getCustomersApi().deleteCustomerByIdWithHttpInfo(id);
    }
}
