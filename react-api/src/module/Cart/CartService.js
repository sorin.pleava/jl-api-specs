import CartsApi from "sdk-js/src/Api/CartsApi";
import ApiService from "../../library/ApiService";

export default class CartService extends ApiService {
    getCartsApi() {
        if (!this.cartsApi) {
            this.cartsApi = new CartsApi(this.apiClient);
        }
        return this.cartsApi;
    }

    getCustomerCart(id) {
        return this.getCartsApi().getCustomerCartWithHttpInfo(id);
    }

    createCart(id, data) {
        return this.getCartsApi().createCartWithHttpInfo(id, data);
    }

    deleteCart(id) {
        return this.getCartsApi().deleteCartWithHttpInfo(id);
    }

    updateCart(id, data) {
        return this.getCartsApi().updateCartWithHttpInfo(id, data);
    }
}
