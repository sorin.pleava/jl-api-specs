import {ApiClient as ApiClientAdapter} from "sdk-js/src";

class ApiClient extends ApiClientAdapter {}

ApiClient.instance = new ApiClient();
export default ApiClient;
