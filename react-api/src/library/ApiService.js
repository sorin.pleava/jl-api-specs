import ApiClient from "./ApiClient";

export default class ApiService {
    constructor() {
        this.apiClient = ApiClient.instance;
        this.apiClient.basePath = 'http://localhost:8000/api/v1';
    }
}