import './App.css';
import React from 'react';
import Shop from "./module/Shop";

function App() {
  return (
      <React.Fragment>
        <Shop />
      </React.Fragment>
  );
}

export default App;
